package com.qinxi1992.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @GetMapping("")
    public String index(){
        return "index";
    }

    @GetMapping("/test")
    public String test(){

        return "test";
    }

    @GetMapping("/test1")
    public String test1(){
        
        return "test1";
    }
}
