package com.qinxi1992.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCIApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCIApplication.class, args);
    }
}
